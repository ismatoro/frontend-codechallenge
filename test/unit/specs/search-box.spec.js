import Vue from 'vue'
import SearchBox from '@/components/search-box'
import { mount } from '@vue/test-utils'

describe('search-box.vue', () => {
  let wrapper;

  beforeEach(function () {
    wrapper = mount(SearchBox)
  })

  it('set search icon if input is empty', () => {
    wrapper.setData({query: ''})
    expect(wrapper.contains('.search-box__icon--search')).to.be.true;
  })

  it('set reset icon if input is not empty', () => {
    wrapper.setData({query: 'test 1'})
    expect(wrapper.contains('.search-box__icon--reset')).to.be.true;
  })

  it('reset input content', () => {
    wrapper.setData({query: 'test'})
    const resetButton = wrapper.find('.search-box__icon--reset')
    resetButton.trigger('click')
    expect(wrapper.find('.search-box__input').isEmpty()).to.be.true;
  })
})

import HomeView from '@/components/home-view'
import { mount } from '@vue/test-utils'

describe('home-view.vue', () => {
  let wrapper;

  beforeEach(function () {
    wrapper = mount(HomeView,{
      // api request called in created hook mocked
      created: function(){ this.items = [
        {id:1, title:"Title test 1", completed:false, userId:1},
        {id:2, title:"Title test 2", completed:true, userId:1},
        {id:3, title:"Title test 3", completed:true, userId:1},
        {id:4, title:"Title test 4", completed:false, userId:1}
      ]}
    })
  })

  it('show title', () => {
    expect(wrapper.text()).to.contain('TO-DO list');
  })

  it('list is shown', () => {
    expect(wrapper.contains('.todo-list')).to.be.true;
  })

  it('list is shown', () => {
    expect(wrapper.contains('.search-box')).to.be.true;
  })

  it('show empty list with query message', () => {
    const emptyWrapper = mount(HomeView,{
      data: function(){return {
        querySearched: 'test'
      }},
      // api request called in created hook mocked
      created: function(){ this.items = []}
    })
    expect(emptyWrapper.text()).to.contain('There is no task with title like');
  })

  it('show empty list with query message', () => {
    const emptyWrapper = mount(HomeView,{
      data: function(){return {
        querySearched: ''
      }},
      // api request called in created hook mocked
      created: function(){ this.items = []}
    })
    expect(emptyWrapper.text()).to.contain('There is no task at this moment');
  })

  it('show empty list network error', () => {
    const emptyWrapper = mount(HomeView,{
      // api request called in created hook mocked
      created: function(){ this.items = null}
    })
    expect(emptyWrapper.text()).to.contain('There is a problem getting tasks');
  })

  // it('hide completed tasks', () => {
  //   expect(wrapper.findAll('.todo-item').length === 4).to.be.true;
  //   const checkbox = wrapper.find('.showChecked__label')
  //   checkbox.trigger('click')
  //   expect(wrapper.findAll('.todo-item').length === 2).to.be.true;
  // })

})

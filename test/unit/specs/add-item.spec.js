import Vue from 'vue'
import AddItem from '@/components/add-item'
import { mount } from '@vue/test-utils'

describe('add-item.vue', () => {
  let wrapper;

  beforeEach(function () {
    wrapper = mount(AddItem)
  })

  it('show form to add item', () => {
    const buttonShow = wrapper.find('.add-item__show-button')
    buttonShow.trigger('click')
    expect(wrapper.contains('.add-item__form')).to.be.true;
  })

  it('show error if input is empty', () => {
    const buttonShow = wrapper.find('.add-item__show-button')
    buttonShow.trigger('click')
    const buttonAdd = wrapper.find('.compound-field__button')
    buttonAdd.trigger('click')
    expect(wrapper.contains('.add-item__error-message')).to.be.true;
  })

})

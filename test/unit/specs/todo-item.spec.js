import Vue from 'vue'
import TodoItem from '@/components/todo-item'
import { mount } from '@vue/test-utils'

describe('todo-item.vue', () => {
  let wrapper;

  beforeEach(function () {
    wrapper = mount(TodoItem,{
      propsData:{
        item:{
          id: 1, title: "test 1", completed: true, userdId: 1
        }
      }
    })
  })

  it('click on item and checkbox change', () => {
    const beforeValue = wrapper.vm.item.completed;
    const item = wrapper.find('.todo-item')
    item.trigger('click')
    return Vue.nextTick()
    .then(function () {
      expect(wrapper.find('input').attributes().value).not.equals(beforeValue);
    })
  })

  it('if item checked, delete button hidden', () => {
    // for the defaul values in props, is checked
    expect(wrapper.contains('todo-item__button')).to.be.false;
  })

})

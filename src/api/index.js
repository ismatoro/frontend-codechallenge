import axios from 'axios';
import config from './config';

function getTasks(){
  return axios.get(config.URL).then(response => response.data);
}

export default {
  getTasks:getTasks
}
